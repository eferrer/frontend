import { AuthModule } from './auth.module';

describe('HeaderModule', () => {
  let headerModule: AuthModule;

  beforeEach(() => {
    headerModule = new AuthModule();
  });

  it('should create an instance', () => {
    expect(headerModule).toBeTruthy();
  });
});
