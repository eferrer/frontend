import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from "../../../core/services/auth.service";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent {

    loginForm: FormGroup;
    errorMessage: string = '';

    constructor(
        private router: Router,
        private authService: AuthService,
        private fb: FormBuilder
    ) {
        this.createForm();
    }

    createForm() {
        this.loginForm = this.fb.group({
            email: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    tryLogin(value) {
        this.authService.doLogin(value)
            .then(res => {
                console.log(res);
                this.router.navigate(['/catalog']);
            }, err => {
                if (err.code === 'auth/user-not-found')
                    this.errorMessage = 'No existe registro del usuario y/o la contraseña';
                else this.errorMessage = err.message;
            });
    }
}
