import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from "../../../core/services/auth.service";

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
    registerForm: FormGroup;
    errorMessage: string = '';
    successMessage: string = '';

    constructor(
        public authService: AuthService,
        private router: Router,
        private fb: FormBuilder
    ) {
        this.createForm();
    }

    createForm() {
        this.registerForm = this.fb.group({
            email: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    tryRegister(value) {
        this.authService.doRegister(value)
            .then(res => {
                this.errorMessage = "";
                this.successMessage = "Cuenta creada satisfactoriamente. Redirigiendo al catalogo ...";

                setTimeout(() => {
                    this.router.navigate(['/catalog']);
                }, 3000);
            }, err => {
                if (err.code === 'auth/email-already-in-use')
                    this.errorMessage = 'El correo ya se encuentra registrado.';
                else this.errorMessage = err.message;

                this.successMessage = "";
            })
    }

}
