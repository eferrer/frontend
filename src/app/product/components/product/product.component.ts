import {Component, OnInit} from '@angular/core';
import {ProductService} from "../../services/product.service";
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
    product: any;

    constructor(
        private route: ActivatedRoute,
        private productService: ProductService
    ) {
        this.product = null;
    }

    ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.loadProduct(params['partNumber']);
        });
    }

    async loadProduct(partNumber: string) {
        try {
            await this.productService.getProduct(partNumber).then( data => {
                if (data['status'] === 'success')
                    this.product = data['data']['product'];
            });
        } catch (e) {
            console.log('Error: ' + e)
        }
    }
}
