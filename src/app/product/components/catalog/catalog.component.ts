import {Component, OnInit} from '@angular/core';
import {ProductService} from "../../services/product.service";

@Component({
    selector: 'app-catalog',
    templateUrl: './catalog.component.html',
    styleUrls: ['./catalog.component.scss']
})
export class CatalogComponent implements OnInit {
    products: any;

    constructor(
        private productService: ProductService
    ) {
        this.products = [];
    }

    ngOnInit(): void {
        this.loadProducts();
    }

    async loadProducts() {
        try {
            await this.productService.getAllProducts().then( data => {
                if (data['status'] === 'success')
                    this.products = data['data']['products'];
            });
        } catch (e) {
            console.log('Error: ' + e)
        }
    }
}
