import {ProductModule} from './product.module';

describe('HeaderModule', () => {
    let headerModule: ProductModule;

    beforeEach(() => {
        headerModule = new ProductModule();
    });

    it('should create an instance', () => {
        expect(headerModule).toBeTruthy();
    });
});
