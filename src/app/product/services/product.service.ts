import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {AuthService} from "../../core/services/auth.service";
import {environment} from "../../../environments/environment";

@Injectable({
    providedIn: 'root'
})
export class ProductService {
    private pathModule = '/products';

    constructor(
        private httpClient: HttpClient,
        private authService: AuthService,
    ) {
    }

    async getAllProducts() {
        const idToken = await this.authService.getCurrentToken();
        let options = {"Accept": "application/json", "idToken": idToken};

        return this.httpClient.get(`${environment.apiUrl}${this.pathModule}`, {
            headers: new HttpHeaders(options)
        }).toPromise();
    }

    async getProduct(partNumber: string) {
        const idToken = await this.authService.getCurrentToken();
        let options = {"Accept": "application/json", "idToken": idToken};

        return this.httpClient.get(`${environment.apiUrl}${this.pathModule}/${partNumber}`, {
            headers: new HttpHeaders(options)
        }).toPromise();
    }

}
