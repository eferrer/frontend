import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {ReactiveFormsModule} from "@angular/forms";
import {CatalogComponent} from "./components/catalog/catalog.component";
import {SharedModule} from "../shared/shared.module";
import {CoreModule} from "../core/core.module";
import {ProductComponent} from "./components/product/product.component";

@NgModule({
    imports: [
        RouterModule,
        ReactiveFormsModule,
        CoreModule,
        SharedModule
    ],
    declarations: [
        CatalogComponent,
        ProductComponent,
    ]
})
export class ProductModule {
}
