import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthModule} from "./auth/auth.module";
import {LoginComponent} from "./auth/components/login/login.component";
import {RegisterComponent} from "./auth/components/register/register.component";
import {AngularFireAuthGuard, redirectLoggedInTo, redirectUnauthorizedTo} from '@angular/fire/auth-guard';
import {AuthService} from "./core/services/auth.service";
import {CatalogComponent} from "./product/components/catalog/catalog.component";
import {ProductModule} from "./product/product.module";
import {ProductComponent} from "./product/components/product/product.component";

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['login']);
const redirectLoggedInToCatalog = () => redirectLoggedInTo(['catalog']);

const routes: Routes = [
    {path: '', redirectTo: 'login', pathMatch: 'full'},
    {
        path: 'login', component: LoginComponent, canActivate: [AngularFireAuthGuard],
        data: {authGuardPipe: redirectLoggedInToCatalog}
    },
    {
        path: 'register', component: RegisterComponent, canActivate: [AngularFireAuthGuard],
        data: {authGuardPipe: redirectLoggedInToCatalog}
    },
    {
        path: 'catalog', component: CatalogComponent, canActivate: [AngularFireAuthGuard],
        data: {authGuardPipe: redirectUnauthorizedToLogin}
    },
    {
        path: 'product/:partNumber', component: ProductComponent, canActivate: [AngularFireAuthGuard],
        data: {authGuardPipe: redirectUnauthorizedToLogin}
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes),
        AuthModule,
        ProductModule
    ],
    providers: [AuthService],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
