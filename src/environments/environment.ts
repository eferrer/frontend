// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firebaseConfig: {
        apiKey: "AIzaSyB4TNDHdHDbvjg3vAiQvjMUXb4_eTM4Ybw",
        authDomain: "desafio-ripley-20200516.firebaseapp.com",
        databaseURL: "https://desafio-ripley-20200516.firebaseio.com",
        projectId: "desafio-ripley-20200516",
        storageBucket: "desafio-ripley-20200516.appspot.com",
        messagingSenderId: "455029129363",
        appId: "1:455029129363:web:0e4cb48583d017cdc7cd66",
        measurementId: "G-G4B30XLCPR"
    },
    apiUrl: '/api/v1'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
