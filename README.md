# Desafío Ripley - Frontend 

Aplicación web con Angular + Firebase

Aplicación web tipo desafío que muestra un catálogo de productos obtenidos desde un backend y los despliega de forma similar al catálogo original de simple.ripley.cl. Adicionalmente si se selecciona un producto se muestra una vista con el detalle del mismo. Para acceder al catálogo es necesario primeramente autenticarse/registrarse con un correo electrónico con Firebase.
 
## Como instalar localmente
1. [Descarga](https://gitlab.com/desafio-ripley/frontend/-/archive/develop/frontend-develop.zip) o clona el [repositorio](https://gitlab.com/desafio-ripley/frontend):
```bash
git clone https://gitlab.com/desafio-ripley/frontend.git
```
2. Entra al directorio del proyecto
```bash
cd frontend
```
3. Instala las dependencias
```bash
npm install
```
4. Actualiza la url de la API backend en el archivo `src/proxy.conf.json`:
```json
{
    "/api/*": {
        "target": "http://127.0.0.1:3000/",
        "secure": false,
        "changeOrigin": true
    }
}
```
5. Inicia el servidor localmente. Navega a `http://localhost:4200/`.
```bash
npm run start
```

## Tecnologías
El proyecto fue creado con:

- Angular 9
- Firebase 7
- Bootstrap 4